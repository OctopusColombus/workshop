package com.batch2.latihan.controller;

import com.batch2.latihan.dto.DosenDTO;
import com.batch2.latihan.model.CustomMappingModel;
import com.batch2.latihan.model.Dosen;
import com.batch2.latihan.model.Mahasiswa;
import com.batch2.latihan.model.Matakuliah;
import com.batch2.latihan.repository.MahasiswaRepository;
import com.batch2.latihan.repository.MatkulRepository;
import com.batch2.latihan.repository.UserRepository;
import com.batch2.latihan.services.CustomQueryDAO;
import com.batch2.latihan.services.DosenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/myApi")
public class MyController {

    @Autowired
    private UserRepository dosenRepo;

    @Autowired
    private MahasiswaRepository mahasiswaRepo;

    @Autowired
    private MatkulRepository matkulRepo;

    @Autowired
    private DosenService dosenService;

    @Autowired
    private CustomQueryDAO ccDao;

    @GetMapping("/getDosen")
    public ResponseEntity<List<Dosen>> getDosen(){
        List<Dosen> result = dosenRepo.findAll();
        return ResponseEntity.ok(result);
    }
    
    @GetMapping("/getDosenDto")
    public ResponseEntity getDosenDto() {
        List<DosenDTO> dosenDtos = dosenService.getDosenDto();
        return ResponseEntity.ok(dosenDtos);
    }

    
    @PostMapping("/saveDataDosen")
    public ResponseEntity<Dosen> saveDosen(@RequestBody Dosen dosen){
        Dosen d = dosenRepo.save(dosen);
        return new ResponseEntity<>(d,HttpStatus.CREATED);
    }
    @PostMapping("/saveDataMatkul")
    public ResponseEntity<Matakuliah> saveMatkul(@RequestBody Matakuliah matkul){
        Matakuliah mk = matkulRepo.save(matkul);
        return new ResponseEntity<>(mk,HttpStatus.CREATED);
    }
    @PostMapping("/saveDataMahasiswa")
    public ResponseEntity<Mahasiswa> SaveMahasiswa(@RequestBody Mahasiswa mhs){
        Mahasiswa mh = mahasiswaRepo.save(mhs);
        return new ResponseEntity<>(mh,HttpStatus.CREATED);
    }
    
    @GetMapping("/nativeQuery")
    public ResponseEntity<List<CustomMappingModel>> getNativeQuery(){
        List<CustomMappingModel> list = ccDao.getCustomQueryNative();
        return ResponseEntity.ok(list);
    }

}
