package com.batch2.latihan.dto;

import java.util.List;

import lombok.Data;

@Data
public class DosenDTO {
  private Long id;
  private String namaDosen;
  private Long nip;
  private String namaMatakuliah;
  private List<String> namaMahasiswa;

}
