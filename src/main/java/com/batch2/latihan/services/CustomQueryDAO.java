package com.batch2.latihan.services;

import com.batch2.latihan.model.CustomMappingModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Service
public class CustomQueryDAO {

    @Autowired
    private EntityManager em;

    public List<CustomMappingModel> getCustomQueryNative(){
        String nativeQueryScript = "select RAND(100) AS id, d.nama_dosen, mh.nama_mhs, mk.nama_matkul\n"+
        "FROM dosen d\n"+
        "INNER JOIN matakuliah mk ON mk.id = d.id_matkul\n"+
        "INNER JOIN mahasiswa mh ON mh.id_dosen =d.id";
        Query q = em.createNativeQuery(nativeQueryScript, "QueryNativePakeJoin");

        List<CustomMappingModel> list = q.getResultList();

        return list;
    }
}
