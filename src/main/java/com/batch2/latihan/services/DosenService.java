package com.batch2.latihan.services;

import com.batch2.latihan.dto.DosenDTO;
import com.batch2.latihan.model.Dosen;
import com.batch2.latihan.repository.UserRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DosenService {

  @Autowired
  private UserRepository userRepository;


  public List<DosenDTO> getDosenDto(){
    List<Dosen> users = userRepository.findAll();
    ModelMapper mm = new ModelMapper();

    return users.stream().map(x -> mm.map(x, DosenDTO.class)).collect(Collectors.toList());
  }
}
