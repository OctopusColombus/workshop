package com.batch2.latihan.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@SqlResultSetMapping(name = "QueryNativePakeJoin", entities = {
        @EntityResult( entityClass = CustomMappingModel.class, fields = {
                @FieldResult(name = "id", column = "id"),
                @FieldResult(name = "namaDosen", column = "nama_dosen"),
                @FieldResult(name = "namaMhs", column = "nama_mhs"),
                @FieldResult(name = "namaMatkul", column = "nama_matkul"),
        })
})
public class CustomMappingModel {
    @Id
    private String id;
    private String namaDosen;
    private String namaMhs;
    private String namaMatkul;
}
