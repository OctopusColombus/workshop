package com.batch2.latihan.model;


import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


import lombok.Data;

@Data
@Entity
@Table(name = "dosen")
public class Dosen {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String namaDosen;
    private Long nip;
    @OneToOne
    private Matakuliah matakuliah;
    
    @OneToMany(mappedBy = "dosen")
    private List<Mahasiswa> mahasiswa;


}
