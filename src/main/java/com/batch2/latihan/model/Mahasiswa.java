package com.batch2.latihan.model;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;

@Data
@Entity
@Table(name = "mahasiswa")
public class Mahasiswa {
    @Id
    private Long id;
    private String namaMhs;
    private String nim;
    
    @ManyToOne
    @JoinColumn(name="id_dosen")
    @JsonBackReference
    private Dosen dosen;

}
