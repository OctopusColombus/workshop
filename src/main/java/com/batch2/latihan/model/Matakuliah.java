package com.batch2.latihan.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;


import lombok.Data;

@Data
@Entity
@Table(name = "matakuliah")
public class Matakuliah {
    
    @Id
    private Long id;
    private String namaMatkul;

    

}
