package com.batch2.latihan.repository;

import com.batch2.latihan.model.Dosen;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Dosen, Long> {

}
