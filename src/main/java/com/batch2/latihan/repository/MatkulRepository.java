package com.batch2.latihan.repository;

import com.batch2.latihan.model.Matakuliah;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MatkulRepository extends JpaRepository<Matakuliah, Long>{
    
}
